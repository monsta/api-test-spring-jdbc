# README #

### 執行條件 ###

* 必須使用JDK1.8+
* 必須使用Tomcat 7.0+
    * 需設定conf/server.xml的URIEncoding為UTF-8，可參考http://tomcat.apache.org/tomcat-8.0-doc/config/http.html#Common_Attributes
* 執行環境需可連外部網路，因有使用CDN下載jquery檔案

### 資料庫資訊 ###
* mysql連線資訊
	* 建立資料庫時schema名稱請訂為bionime
	* 帳號密碼預設使用"test"
	* 請使用src/main/resources/db/migration/V1_init.sql檔案來建立相關table schema
	* 若有需要調整資料庫連線參數，可在war檔解開後，於WEB-INF/classes/persistence-mysql.properties修改資料庫連線參數
