package lottery.service;

import lottery.model.Station;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * BionimeServiceIntegrationTest description
 *
 * @author johnson
 * @since 2015-03-02
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class BionimeServiceIntegrationTestOnStation extends AbstractJpaRollbackIntegrationTest {

    private static final Logger LOG = LoggerFactory.getLogger(BionimeServiceIntegrationTestOnStation.class);

    @Test
    public void whenContextIsBooted_thenNoExceptions() {
    }

    @Test
    @Transactional
    public void whenEntityIsCreated_thenNoExceptions() {
        bionimeService.addStation("ER");
    }

    @Test
    @Transactional
    public void createAndGetAllStation_shouldGetListOfStation() {
        this.bionimeService.addStation("stationA");
        this.bionimeService.addStation("stationB");

        List<Station> allStation = this.bionimeService.getAllStation();
        assertThat(allStation.size()).isGreaterThan(1);
        for (Station station : allStation) {
            assertThat(station.getId()).isNotNull();
            assertThat(station.getName()).isNotEmpty();
            assertThat(station.getUpdatetime()).isNotNull();
        }
    }

    @Test
    public void queryTestData_shouldBeSuccess() {
        final Query query = em.createQuery("From Station");
        List<Station> result = query.getResultList();
        LOG.warn("query finished~~");

        Station targetStation = result.get(result.size() - 1);
        Integer targetStationId = targetStation.getId();
        String expectedName = targetStation.getName();

        Station station = this.bionimeService.getStation(targetStationId);
        assertThat(station.getName()).isEqualTo(expectedName);
    }

    @Test
    @Transactional
    public void updateStationName_noExceptions() {
        bionimeService.addStation("OOO");
        Query query = em.createQuery("From Station WHERE name='OOO'");
        Station targetStation = (Station) query.getResultList().get(0);

        String newName = "AAA";
        this.bionimeService.updateStationName(targetStation.getId(), newName);

        Station stationAfterUpdated = this.bionimeService.getStation(targetStation.getId());
        assertThat(stationAfterUpdated.getName())
                .isEqualTo(newName);
    }

    @Test
    @Transactional
    public void deleteStation_shouldSuccess() {
        String targetStationName = "deleted";
        this.bionimeService.addStation(targetStationName);

        Query query = em.createQuery("From Station WHERE name='deleted'");
        List<Station> result = query.getResultList();
        Station tobeDeletedStation = result.get(0);
        assertThat(tobeDeletedStation.getName()).isEqualTo(targetStationName);

        this.bionimeService.deleteStation(tobeDeletedStation.getId());
        Query query2 = em.createQuery("From Station WHERE name='deleted'");
        List result2 = query2.getResultList();
        assertThat(result2).isEmpty();
    }
}
