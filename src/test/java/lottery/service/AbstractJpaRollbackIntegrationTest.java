package lottery.service;

import lottery.config.DataConfig;
import lottery.config.SpringConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * class description of AbstractJpaRollbackIntegrationTest
 *
 * @author johnson
 */
@ContextConfiguration(classes = {SpringConfig.class, DataConfig.class})
@TransactionConfiguration
public abstract class AbstractJpaRollbackIntegrationTest {

    @PersistenceContext
    protected EntityManager em;

    @Autowired
    protected BionimeService bionimeService;
}
