package lottery.service;

import lottery.dao.INurseDao;
import lottery.model.Nurse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * class description of BionimeServiceTestOnNurse
 *
 * @author johnson
 */
@RunWith(MockitoJUnitRunner.class)
public class BionimeServiceTestOnNurse {

    @InjectMocks
    private BionimeService bionimeService;

    @Mock
    private INurseDao nurseDaoMock;

    @Before
    public void setUp() {
        this.bionimeService = new BionimeService();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllNurseTest() {
        this.bionimeService.getAllNurse();

        verify(nurseDaoMock, times(1)).findAll();
    }

    @Test
    public void getNurseTest() {
        Integer nurseId = 1;
        this.bionimeService.getNurse(nurseId);

        verify(nurseDaoMock, times(1)).findOne(nurseId);
    }

    @Test
    public void deleteNurseTest() {
        Integer nurseId = 2;
        when(nurseDaoMock.findOne(nurseId)).thenReturn(new Nurse());
        this.bionimeService.deleteNurse(nurseId);

        InOrder order = inOrder(nurseDaoMock);
        order.verify(nurseDaoMock, times(1)).findOne(nurseId);
        order.verify(nurseDaoMock, times(1)).delete(any(Nurse.class));
    }

    @Test
    public void addNurseTest() {
        final String nurseNo = "A123";
        final String nurseName = "JJ";
        final List<Integer> stationSelect = Collections.emptyList();
        this.bionimeService.addNurse(nurseNo, nurseName, stationSelect);

//        InOrder order = inOrder(stationDaoMock, nurseDaoMock);
        verify(nurseDaoMock, times(1)).create(any(Nurse.class));
    }
}
