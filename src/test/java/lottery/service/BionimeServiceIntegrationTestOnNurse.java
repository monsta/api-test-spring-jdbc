package lottery.service;

import lottery.model.Nurse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * BionimeServiceIntegrationTest description
 *
 * @author johnson
 * @since 2015-03-02
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class BionimeServiceIntegrationTestOnNurse extends AbstractJpaRollbackIntegrationTest {

    private static final Logger LOG = LoggerFactory.getLogger(BionimeServiceIntegrationTestOnNurse.class);

    @Test
    @Transactional
    public void whenEntityIsCreated_thenNoExceptions() {
        bionimeService.addNurse("no", "name", Arrays.asList(1, 2));
    }

    @Test
    @Transactional
    public void createAndGetAllNurse_shouldGetListOfNurse() {
        List<Integer> stationList = Arrays.asList(1, 2);
        this.bionimeService.addNurse("A123", "nurse1", stationList);
        this.bionimeService.addNurse("A124", "nurse2", stationList);

        List<Nurse> allallNurse = this.bionimeService.getAllNurse();
        assertThat(allallNurse.size()).isGreaterThan(1);
        for (Nurse nurse : allallNurse) {
            assertThat(nurse.getId()).isNotNull();
            assertThat(nurse.getName()).isNotEmpty();
            assertThat(nurse.getNo()).isNotEmpty();
        }
    }

    @Test
    @Transactional
    public void updateNurseName_shouldSuccess() {
        List<Integer> stations = Arrays.asList(1, 2);
        bionimeService.addNurse("A111", "Jone", stations);
        Query query = em.createQuery("From Nurse WHERE name='Jone'");
        Nurse targetNurse = (Nurse) query.getResultList().get(0);

        String newName = "Jane";
        this.bionimeService.updateNurse(targetNurse.getId(), targetNurse.getNo(), newName, stations);

        Nurse updatedNurse = this.bionimeService.getNurse(targetNurse.getId());
        assertThat(updatedNurse.getName())
                .isEqualTo(newName);
    }

    @Test
    @Transactional
    public void updateNurseNo_shouldSuccess() {
        List<Integer> stations = Arrays.asList(1, 2);
        bionimeService.addNurse("A111", "Jone", stations);
        Query query = em.createQuery("From Nurse WHERE no='A111'");
        Nurse targetNurse = (Nurse) query.getResultList().get(0);

        String newNo = "A112";
        this.bionimeService.updateNurse(targetNurse.getId(), newNo, targetNurse.getName(), stations);

        Nurse updatedNurse = this.bionimeService.getNurse(targetNurse.getId());
        assertThat(updatedNurse.getNo())
                .isEqualTo(newNo);
    }

    @Test
    @Transactional
    public void deleteStation_shouldSuccess() {
        String targetNurseName = "Jade";
        this.bionimeService.addNurse("A222", targetNurseName, Arrays.asList(1, 2));

        Query query = em.createQuery("From Nurse WHERE name='Jade'");
        List<Nurse> result = query.getResultList();
        //確認只有一筆Jade
        assertThat(result.size()).isEqualTo(1);
        Nurse toBeDeletedNurse = result.get(0);
        assertThat(toBeDeletedNurse.getName()).isEqualTo(targetNurseName);

        this.bionimeService.deleteNurse(toBeDeletedNurse.getId());
        Query query2 = em.createQuery("From Nurse WHERE name='Jade'");
        List result2 = query2.getResultList();
        assertThat(result2).isEmpty();
    }
}
