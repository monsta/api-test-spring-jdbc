package lottery.service;

import lottery.dao.IStationDao;
import lottery.model.Station;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * class description of BionimeServiceTest
 *
 * @author johnson
 */
@RunWith(MockitoJUnitRunner.class)
public class BionimeServiceTestOnStation {

    @InjectMocks
    private BionimeService bionimeService;

    @Mock
    private IStationDao dao;

    @Before
    public void setUp() {
        this.bionimeService = new BionimeService();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllStationShouldReturnList() {
        when(dao.findAll()).thenReturn(new ArrayList<>());
        assertThat(bionimeService.getAllStation()).isInstanceOf(List.class);
    }

    @Test
    public void getStationById() {
        when(dao.findOne(anyInt())).thenReturn(mock(Station.class));
        assertThat(bionimeService.getStation(1)).isInstanceOf(Station.class);
    }

    @Test
    public void updateStationNameTest() {
        when(dao.findOne(anyInt())).thenReturn(mock(Station.class));
        assertThat(bionimeService.updateStationName(1, "newStationName")).isTrue();
    }
}
