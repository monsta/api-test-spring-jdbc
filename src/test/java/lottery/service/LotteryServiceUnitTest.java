package lottery.service;

import lottery.dao.ILotteryBetformDAO;
import lottery.model.BetForm;
import lottery.util.BetformCalculator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * LotteryServiceUnitTest description
 *
 * @author johnson
 * @since 2015-06-22
 */
@RunWith(MockitoJUnitRunner.class)
public class LotteryServiceUnitTest {

    @Mock
    private ILotteryBetformDAO betformDAO;

    @Mock
    private BetformCalculator betformCalculator;

    private LotteryService lotteryService;

    @Before
    public void setUp() {
        lotteryService = new LotteryService(betformDAO, betformCalculator);
    }

    @Test
    public void processBetform_shouldCallDAOMethods() throws IOException {
        String betFormId = "1";
        BetForm stubBetForm = getStubBetForm();
        when(betformDAO.findByBetFormId(Integer.valueOf(betFormId)))
            .thenReturn(stubBetForm);

        lotteryService.processBetform(betFormId);

        verify(betformDAO, times(1)).findByBetFormId(Integer.valueOf(betFormId));
        verify(betformCalculator, times(1)).unionCombination(stubBetForm.getBetItem(),
                                                            stubBetForm.getBetId());
    }

    private BetForm getStubBetForm() {
        BetForm stubBetForm = new BetForm();
        //假設為連碰
        stubBetForm.setBetModeId((short)1);
        stubBetForm.setBetItem("");
        int betId = 5;
        stubBetForm.setBetId((short) betId);
        return stubBetForm;
    }
}
