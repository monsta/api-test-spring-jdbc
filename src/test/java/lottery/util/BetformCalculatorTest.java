package lottery.util;

import lottery.model.Combination;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * BetformCalculatorTest description
 *
 * @author johnson
 * @since 2015-06-26
 */
public class BetformCalculatorTest {

    private BetformCalculator calculator;

    @Before
    public void setUp() {
        this.calculator = new BetformCalculator();
    }

    @Test
    public void unionCombination() throws IOException {
        //取得某個betItem的2星組合
        List<Combination> combResult = calculator.unionCombination(
            "[\"03\",\"04\",\"05\",\"06\",\"09\",\"10\",\"11\",\"14\",\"15\",\"16\",\"17\"," +
                "\"20\",\"21\",\"22\",\"25\",\"26\",\"27\",\"28\",\"31\",\"32\",\"33\",\"36\"," +
                "\"37\",\"38\",\"39\",\"41\",\"42\",\"47\",\"48\",\"49\"]",
            (short) 6);

        assertThat(combResult).isNotEmpty();
        combResult.forEach(comb -> {
            System.out.println(comb.toString());
        });
    }

    /**
     * Jackson Mapper太好用了!!
     * @throws IOException
     */
    @Test
    public void testNumberStringArrayToIntegerSet() throws IOException {
        String unionBetItem = "[\"03\",\"04\",\"05\",\"06\",\"09\",\"10\",\"11\",\"14\"" +
            ",\"15\",\"16\",\"17\",\"20\",\"21\",\"22\",\"25\",\"26\",\"27\",\"28\",\"31\"" +
            ",\"32\",\"33\",\"36\",\"37\",\"38\",\"39\",\"41\",\"42\",\"47\",\"48\",\"49\"]";
        ObjectMapper mapper = new ObjectMapper();
        Set<Integer> result = mapper.readValue(unionBetItem,
                                                new TypeReference<Set<Integer>>(){});
        System.out.println(result);
    }

    /**
     * Jackson Mapper也太神奇!!
     * @throws IOException
     */
    @Test
    public void test2DArray() throws IOException {
        String betItem = "[[\"01\",\"11\",\"21\",\"31\",\"41\"],[\"04\",\"14\",\"24\",\"34\"," +
            "\"44\"],[\"05\",\"15\",\"25\",\"35\",\"45\"]]";

        ObjectMapper mapper = new ObjectMapper();
        List<Set<Integer>> resutl = mapper.readValue(betItem, new TypeReference<List<Set<Integer>>>() {
        });
        System.out.println(resutl);
    }
}
