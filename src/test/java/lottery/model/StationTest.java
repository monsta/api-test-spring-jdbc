package lottery.model;

import org.junit.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * StationTest description
 *
 * @author johnson
 * @since 2015-03-02
 */
public class StationTest {

    @Test
    public void getUpdatetimeFormated_shouldSuccess() {
        Station station = new Station();
        station.setUpdatetime(new Date());

        String formatedDatetimeString = station.getUpdatetimeFormated();
        assertThat(formatedDatetimeString).isNotEmpty();
        System.out.println(formatedDatetimeString);
    }
}
