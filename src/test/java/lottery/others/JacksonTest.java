package lottery.others;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * JacksonTest description
 *
 * @author johnson
 * @since 2015-06-26
 */
public class JacksonTest {

    @Test
    public void mapToJson() throws IOException {
        Map<String, String> stubMap = new HashMap<>();
        stubMap.put("1", "true");
        stubMap.put("2", "false");

        String str = new ObjectMapper().writeValueAsString(stubMap);
        assertThat(str).contains("\"1\":\"true\"");
    }
}
