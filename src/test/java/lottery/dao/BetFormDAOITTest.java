package lottery.dao;

import lottery.config.DataConfig;
import lottery.config.SpringConfig;
import lottery.model.BetForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * BetFormDAOITTest description
 *
 * @author johnson
 * @since 2015-06-26
 */
@IfProfileValue(name = "test.category", value = "integration")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfig.class, DataConfig.class})
@TransactionConfiguration
public class BetFormDAOITTest {

    @Autowired
    private ILotteryBetformDAO betformDAO;

    @Test
    public void findByBetformId() {
        int betFormId = 1;
        BetForm betform = betformDAO.findByBetFormId(betFormId);
        assertThat(betform).isNotNull();
        assertThat(betform.getBetFormId()).isEqualTo(betFormId);
        assertThat(betform.getBetItem()).isNotEmpty();
        assertThat(betform.getScheduleId()).isNotNull();
        assertThat(betform.getEachPayable()).isNotNegative();
        assertThat(betform.getEachUnit()).isNotNull();
        assertThat(betform.getRate()).isNotNull();
        assertThat(betform.getRateModify()).isNotNull();
        assertThat(betform.getRateNotice()).isNotNull();
        assertThat(betform.getBetOptionClosed()).isNotNull();
        assertThat(betform.getUpCEO()).isNotNull();
        assertThat(betform.getDownCEO()).isNotNull();
        assertThat(betform.getUpShareown()).isNotNull();
        assertThat(betform.getDownShareown()).isNotNull();
        assertThat(betform.getUpAgent()).isNotNull();
        assertThat(betform.getDownAgent()).isNotNull();
        assertThat(betform.getPercentUpCEO()).isNotNull().isNotNegative();
        assertThat(betform.getPercentDownCEO()).isNotNull().isNotNegative();
        assertThat(betform.getPercentUpShareown()).isNotNull().isNotNegative();
        assertThat(betform.getPercentDownShareown()).isNotNull().isNotNegative();
        assertThat(betform.getPercentUpAgent()).isNotNull().isNotNegative();
        assertThat(betform.getPercentDownAgent()).isNotNull().isNotNegative();
        assertThat((betform.getUnitLimit())).isNotNull();
        assertThat(betform.getBetModeId()).isNotNull();
    }
}
