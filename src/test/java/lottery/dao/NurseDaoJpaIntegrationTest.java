package lottery.dao;

import lottery.config.DataConfig;
import lottery.config.SpringConfig;
import lottery.model.Nurse;
import lottery.model.Station;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * class description of NurseDaoJpaIntegrationTest
 *
 * @author johnson
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfig.class, DataConfig.class})
@TransactionConfiguration
public class NurseDaoJpaIntegrationTest {

    @PersistenceContext
    EntityManager em;

    @Autowired
    private INurseDao nurseDao;

    @Autowired
    private IStationDao stationDao;

    @Test
    @Transactional
    public void addNurseWithStations_shouldSuccess() {
        Query deleteStationNurse = em.createNativeQuery("delete from station_nurse");
        deleteStationNurse.executeUpdate();
        Query deleteAllNurseQuery = em.createNativeQuery("delete from nurse");
        deleteAllNurseQuery.executeUpdate();

        Nurse nurse = new Nurse();
        nurse.setName("nurse");
        nurse.setNo("A001");

        Set<Station> stations = new HashSet<>(stationDao.findAll());
        int allStationSize = stations.size();
        nurse.setStations(stations);

        nurseDao.create(nurse);

        Query nativeQuery = em.createNativeQuery("select * from station_nurse");
        List resultList = nativeQuery.getResultList();

        assertThat(resultList.size()).isEqualTo(allStationSize);
    }

}
