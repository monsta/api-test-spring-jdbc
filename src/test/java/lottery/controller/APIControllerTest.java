package lottery.controller;

import lottery.service.LotteryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * APIControllerTest description
 *
 * @author johnson
 * @since 2015-06-22
 */
@RunWith(MockitoJUnitRunner.class)
public class APIControllerTest {

    private MockMvc mockMvc;

    @Mock
    private LotteryService lotteryService;

    @Before
    public void setUp() {
        this.mockMvc = standaloneSetup(new APIController(lotteryService))
            .build();
    }

    @Test
    public void lotteryLimitTest() throws Exception {
        String input = "{\"1\":\"{01x02x03:40.0000}\",\"2\":\"\",\"3\":\"{01x02x04:40.0000}\"}";
        Map<String, Object> stubMap = new HashMap<>();
        stubMap.put("1", "xxxx");

        this.mockMvc.perform(post("/ll")
            .param("input", input))
            .andExpect(status().isOk())
            .andExpect(content().string("test"));

        verify(lotteryService, times(1)).processBetform("1");
    }
}
