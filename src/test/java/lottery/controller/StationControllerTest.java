package lottery.controller;

import lottery.model.Station;
import lottery.service.BionimeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author johnson
 */
@RunWith(MockitoJUnitRunner.class)
public class StationControllerTest {

    private MockMvc mockMvc;

    @Mock
    private BionimeService bionimeServiceMock;

    @Before
    public void setUp() {
        this.mockMvc = standaloneSetup(new StationController(bionimeServiceMock))
                .build();
    }

    @Test
    public void newStationViewNameTest() throws Exception {
        this.mockMvc.perform(get("/station/new"))
                .andExpect(view().name("newStation"));
    }

    @Test
    public void listStationTest() throws Exception {
        this.mockMvc.perform(get("/station/list"))
                .andExpect(view().name("listStation"))
                .andExpect(model().attribute("stationList", instanceOf(List.class)));
        verify(bionimeServiceMock, times(1)).getAllStation();
    }

    @Test
    public void addNewStation() throws Exception {
        when(bionimeServiceMock.addStation(anyString()))
                .thenReturn(true);
        this.mockMvc.perform(
                post("/station/new")
                    .param("stationName", "新站點"))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/station/list"))
                .andExpect(flash().attribute("message", is(FlashMessage.NEW_STATION_SUCCESS)));
    }

    @Test
    public void viewStationTest() throws Exception {
        when(bionimeServiceMock.getStation(anyInt()))
                .thenReturn(mock(Station.class));
        this.mockMvc.perform(get("/station/view/{stationId}", 1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("viewStation"))
                .andExpect(model().attributeExists("station"))
                .andExpect(model().attribute("station", instanceOf(Station.class)));
    }

    @Test
    public void updateStationTest() throws Exception {
        int id = 1;
        this.mockMvc.perform(
                post("/station/update/{stationId}", id)
                    .param("stationName", "新站點名稱"))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/station/list"));
    }

    @Test
    public void deleteStationTest() throws Exception {
        this.mockMvc.perform(
                post("/station/delete/{stationId}", 1))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/station/list"));
    }
}
