package lottery.controller;

import lottery.model.Nurse;
import lottery.service.BionimeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * class description of NurseControllerTest
 *
 * @author johnson
 */
@RunWith(MockitoJUnitRunner.class)
public class NurseControllerTest {

    private MockMvc mockMvc;

    @Mock
    private BionimeService bionimeServiceMock;

    @Before
    public void setUp() {
        this.mockMvc = standaloneSetup(new NurseController(bionimeServiceMock))
                .build();
    }

    @Test
    public void listNursesTest() throws Exception {
        this.mockMvc.perform(get("/nurse/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("listNurse"))
                .andExpect(model().attributeExists("nurseList"))
                .andExpect(model().attribute("nurseList", instanceOf(List.class)));

        verify(bionimeServiceMock, times(1)).getAllNurse();
    }

    @Test
    public void toNewNursePageTest() throws Exception {
        this.mockMvc.perform(get("/nurse/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("newNurse"))
                .andExpect(model().attributeExists("stationList"));
        verify(bionimeServiceMock, times(1)).getAllStation();
    }

    @Test
    public void addNewNurse() throws Exception {
        String no = "A123";
        String name = "Jone";
        Integer[] stationSelect = {1, 2};
        List<Integer> stationSelectList = Arrays.asList(stationSelect);

        this.mockMvc.perform(
                post("/nurse/new")
                        .param("no", no)
                        .param("name", name)
                        .param("stationSelect", "1", "2"))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/nurse/list"))
                .andExpect(flash().attribute("message", instanceOf(FlashMessage.class)));
    }

    @Test
    public void viewNurseTest() throws Exception {
        Integer nurseId = 1;
        when(bionimeServiceMock.getNurse(nurseId))
                .thenReturn(new Nurse("A111", "Jone"));

        this.mockMvc.perform(get("/nurse/view/{nurseId}", nurseId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("viewNurse"))
                .andExpect(model().attributeExists("nurse"))
                .andExpect(model().attribute("nurse", instanceOf(Nurse.class)));

        verify(bionimeServiceMock, times(1)).getNurse(nurseId);
    }

    @Test
    public void updateNurseTest() throws Exception {
        String no = "A112";
        String name = "Jone";
        Integer[] stationSelect = {2, 3};
        Integer nurseId = 1;
        when(bionimeServiceMock.updateNurse(nurseId, no, name, Arrays.asList(stationSelect)))
                .thenReturn(true);

        this.mockMvc.perform(
                post("/nurse/update/{nurseId}", nurseId)
                        .param("no", no)
                        .param("name", name)
                        .param("stationSelect", "2", "3"))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/nurse/list"))
                .andExpect(flash().attribute("message", FlashMessage.UPDATE_NURSE_SUCCESS));

        verify(bionimeServiceMock, times(1)).updateNurse(nurseId, no, name, Arrays.asList(stationSelect));
    }

    @Test
    public void deleteNurseTest() throws Exception {
        Integer nurseId = 2;

        this.mockMvc.perform(post("/nurse/delete/{nurseId}", nurseId))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/nurse/list"))
                .andExpect(flash().attribute("message", FlashMessage.DEL_NURSE_SUCCESS));

        verify(bionimeServiceMock, times(1))
                .deleteNurse(nurseId);
    }

//    @Test
    public void checkNurseNoDuplicatedTest() throws Exception {
        this.mockMvc.perform(post("/nurse/checkDupNo")
                .param("no", "A100"))
                .andDo(print())
                .andExpect(status().isOk());
//                .andExpect(jsonPath())
    }
}
