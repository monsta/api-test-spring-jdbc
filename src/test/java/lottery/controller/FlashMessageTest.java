package lottery.controller;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * class description of FlashMessageTest
 *
 * @author johnson
 */
public class FlashMessageTest {

    @Test
    public void getMessageStringTest() {
        assertThat(
                FlashMessage.DEL_NURSE_SUCCESS.getMessageString())
                .isNotEmpty();
        assertThat(
                FlashMessage.NEW_STATION_FAIL.getMessageString())
                .isNotEmpty();
    }
}
