package lottery.controller;

import org.junit.Test;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class IndexControllerTest {

    @Test
    public void indexViewNameTest() throws Exception {
        MockMvcBuilders.standaloneSetup(new IndexController())
                .build()
                .perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }
}
