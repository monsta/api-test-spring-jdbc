package lottery.controller;

import lottery.config.MvcConfig;
import lottery.config.SpringConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

/**
 * for integration testing controllers
 * not used for now...
 */
@WebAppConfiguration
@ContextConfiguration(classes = {SpringConfig.class, MvcConfig.class})
public abstract class AbstractContextControllerTest {

    @Autowired
    protected WebApplicationContext wac;
}
