<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>list nurse</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap.min.css"/>'>
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap-theme.min.css"/>'>
</head>
<body>
<div id="function">
        <span>
            <button value="返回" id="returnBtn">返回</button>
        </span>
</div>
<c:if test="${not empty message}">
<div class="message">
    ${message.messageString}
</div>
</c:if>
<div id="stationListTable" class="col-md-6">
    <c:if test="${not empty nurseList}">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>員編</th>
                <th>修改時間</th>
                <th>動作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${nurseList}" var="nurse">
                <tr>
                    <td>${nurse.no}</td>
                    <td>${nurse.updatetimeFormated}</td>
                    <td>
                        <span>
                            <a href='<spring:url value="/nurse/view/${nurse.id}"/>'>View</a>
                        </span>
                        <span>
                            <a href="#" class="delNurseLink" rel="${nurse.id}">Del</a>
                        </span>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <c:if test="${empty nurseList}">
        <div class="alert alert-warning" role="alert">
            <spring:message code="noNusre"/>
        </div>
    </c:if>
</div>

<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src='<spring:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>

<script>
    $(function () {
        $('#returnBtn').click(function () {
            window.location = '<spring:url value="/"/>';
        });

        $('.delNurseLink').click(function () {
            if (window.confirm('<spring:message code="confirmMsg"/>')) {
                $.post(
                        '<spring:url value="/nurse/delete/"/>/' + $(this).attr("rel"),
                        function () {
                            window.location.replace('<spring:url value="/nurse/list"/>');
                        }
                );
            }
        });
    });
</script>
</body>
</html>
