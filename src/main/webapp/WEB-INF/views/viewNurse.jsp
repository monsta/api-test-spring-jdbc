<%@ page import="java.util.Set" %>
<%@ page import="java.util.List" %>
<%@ page import="lottery.model.Station" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>view nurse</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap.min.css"/>'>
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap-theme.min.css"/>'>
    <link rel="stylesheet" media="screen" type="text/css" href='<c:url value="/resources/multiselect/css/multi-select.css"/>'>
</head>
<body>
    <div id="function">
        <span>
            <button value="返回" id="returnBtn">返回</button>
        </span>
        <span>
            <button value="新增" id="updateBtn">儲存</button>
        </span>
    </div>
    <form id="form" method="post" action='<spring:url value="/nurse/update/${nurse.id}"/>'>
        <div id="stationContent">
            <label for="no">員工編號</label>
            <input id="no" name="no" type="text" size="10" maxlength="10" value='${nurse.no}'>
            <br/>
            <label for="name">護士姓名</label>
            <input id="name" name="name" type="text" size="10" maxlength="10" value='${nurse.name}'>
        </div>
        <div id="stationSelection" class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">分配站點</h3>
                </div>
                <div class="panel-body">
                    直接點選項目做加入/移除
                </div>
            </div>
            <%--<span>分配站點：直接點選項目做加入/移除</span>--%>
            <select multiple="multiple" id="stationSelect" name="stationSelect">
            <%
                List<Station> stationList = (List<Station>) request.getAttribute("stationList");
                Set<Integer> nurseStationsId = (Set<Integer>) request.getAttribute("nurseStationsId");
                for (Station station : stationList) {
                    if (nurseStationsId.contains(station.getId())) {
            %>
                <option value="<%=station.getId() %>" selected><%=station.getName() %>
                </option>
                <%
                } else {
                %>
                <option value="<%=station.getId() %>"><%=station.getName() %>
                </option>
                <%
                        }
                    }
            %>
            </select>
        </div>
    </form>

    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src='<c:url value="/resources/multiselect/js/jquery.multi-select.js"/>'></script>
    <script src='<spring:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>

    <script>
        $(function() {
            $('#updateBtn').click(function() {
                $('#form').submit();
            });
            $('#returnBtn').click(function() {
                window.location = '<spring:url value="/nurse/list"/>';
            });
            $('#stationSelect').multiSelect({
                keepOrder: true,
                selectableHeader: "<div class='custom-header'>未加入</div>",
                selectionHeader: "<div class='custom-header'>已加入</div>"
            });
        });
    </script>
</body>
</html>
