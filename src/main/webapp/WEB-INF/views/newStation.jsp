<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>new station</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap.min.css"/>'>
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap-theme.min.css"/>'>
</head>
<body>
    <div id="function">
        <span>
            <button value="返回" id="returnBtn">返回</button>
        </span>
        <span>
            <button value="新增" id="confirmBtn">新增</button>
        </span>
    </div>
    <div id="stationContent" class="col-md-6">
        <form id="form" method="post" action='<spring:url value="/station/new"/>'>
            <label for="stationName">站點名稱</label>
            <input id="stationName" name="stationName" type="text" size="10" maxlength="10">
        </form>
    </div>

    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src='<spring:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>

    <script>
        $(function() {
            $('#confirmBtn').click(function() {
                $('#form').submit();
            });
            $('#returnBtn').click(function() {
                window.location = '<spring:url value="/"/>';
            });
        });
    </script>
</body>
</html>
