<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>view station</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap.min.css"/>'>
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap-theme.min.css"/>'>
</head>
<body>
    <div id="function">
        <span>
            <button value="返回" id="returnBtn">返回</button>
        </span>
        <span>
            <button value="新增" id="updateBtn">儲存</button>
        </span>
    </div>
    <div id="stationContent">
        <form id="form" method="post" action='<spring:url value="/station/update/${station.id}"/>'>
            <label for="stationName">站點名稱</label>
            <input id="stationName" name="stationName" type="text" size="10" maxlength="10" value='${station.name}'>
        </form>
    </div>
    <div id="nurseList" class="col-md-6">
        <span>站點護士列表(test)</span>
        <c:if test="${not empty station.nurses}">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>員工編號</th>
                    <th>加入時間</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${station.nurses}" var="nurse">
                    <tr>
                        <td>${nurse.no}</td>
                        <td>${nurse.addStationTimeFormated}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>
        <c:if test="${empty station.nurses}">
            <span>
                <spring:message code="noNurseIn"/>
            </span>
        </c:if>
    </div>

    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src='<spring:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>

    <script>
        $(function() {
            $('#updateBtn').click(function() {
                $('#form').submit();
            });
            $('#returnBtn').click(function() {
                window.location = '<spring:url value="/station/list"/>';
            });
        });
    </script>
</body>
</html>
