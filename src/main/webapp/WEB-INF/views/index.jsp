<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta charset="utf-8">
    <title>index</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap.min.css"/>'>
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap-theme.min.css"/>'>
</head>
<body>
    <div class="container theme-showcase"id="main">
        <ul>
            <li>
                <span>
                    <button type="button" class="btn btn-lg btn-link">
                        <a href='<spring:url value="/station/new"/>'>新增站點</a>
                    </button>
                </span>
            </li>
            <li>
                <span>
                    <button type="button" class="btn btn-lg btn-link">
                        <a href='<spring:url value="/station/list"/>'>站點列表</a>
                    </button>
                </span>
            </li>
            <li>
                <span>
                    <button type="button" class="btn btn-lg btn-link">
                        <a href='<spring:url value="/nurse/new"/>'>新增護士</a>
                    </button>
                </span>
            </li>
            <li>
                <span>
                    <button type="button" class="btn btn-lg btn-link">
                        <a href='<spring:url value="/nurse/list"/>'>護士列表</a>
                    </button>
                </span>
            </li>
        </ul>
    </div>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src='<spring:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>
</body>
</html>
