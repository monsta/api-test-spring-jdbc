<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>list station</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap.min.css"/>'>
    <link rel="stylesheet" href='<spring:url value="/resources/bootstrap/css/bootstrap-theme.min.css"/>'>
</head>
<body>
    <div id="function">
        <span>
            <button value="返回" id="returnBtn">返回</button>
        </span>
    </div>
    <div id="stationListTable" class="col-md-6">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>站點</th>
                    <th>修改時間</th>
                    <th>動作</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${stationList}" var="station">
                <tr>
                    <td>${station.name}</td>
                    <td>${station.updatetimeFormated}</td>
                    <td>
                        <span>
                            <a href='<spring:url value="/station/view/${station.id}"/>'>View</a>
                        </span>
                        <span>
                            <a href="#" class="delStationLink" rel="${station.id}">Del</a>
                        </span>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src='<spring:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>

    <script>
        $(function() {
            $('#returnBtn').click(function() {
                window.location = '<spring:url value="/"/>';
            });

            $('.delStationLink').click(function() {
                if(window.confirm('<spring:message code="confirmMsg"/>')) {
                    $.post('<spring:url value="/station/delete/"/>/'+$(this).attr("rel"), function() {
                        window.location.replace('<spring:url value="/station/list"/>');
                    });
                }
            });
        });
    </script>
</body>
</html>
