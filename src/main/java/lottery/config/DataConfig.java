package lottery.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * JPConfig description
 *
 * @author johnson
 * @since 2015-03-02
 */
@Configuration
@PropertySource({"classpath:persistence-mysql.properties"})
@EnableTransactionManagement
@ComponentScan({"lottery.dao"})
public class DataConfig {

    @Autowired
    private Environment env;

    /* for JPA
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(datasource());
        emf.setPackagesToScan("lottery.model");

        JpaVendorAdapter vendor = new HibernateJpaVendorAdapter();
        emf.setJpaVendorAdapter(vendor);
        emf.setJpaProperties(additionalProperties());

        return emf;
    }*/

    @Bean
    public DataSource datasource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");

        dataSource.setMaximumPoolSize(Integer.parseInt(env.getProperty("dataSource" +
            ".maximumPoolSize")));
        dataSource.addDataSourceProperty("url", env.getProperty("dataSource.url"));
        dataSource.addDataSourceProperty("user", env.getProperty("jdbc.user"));
        dataSource.addDataSourceProperty("password", env.getProperty("jdbc.password"));

//        dataSource.setInitializationFailFast(true);
        dataSource.addDataSourceProperty("prepStmtCacheSize", env.getProperty("dataSource" +
            ".prepStmtCacheSize"));
        dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", env.getProperty("dataSource" +
            ".prepStmtCacheSqlLimit"));
        dataSource.addDataSourceProperty("cachePrepStmts", env.getProperty("dataSource" +
            ".cachePrepStmts"));

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager
            (datasource());
        return transactionManager;
    }

    /* for JPA
    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }*/

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTransaltor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /* for JPA
    private Properties additionalProperties() {
        Properties prop = new Properties();
        prop.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
        prop.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        prop.setProperty("hibernate.format_sql", env.getProperty("hibernate.format_sql"));
        return prop;
    }*/
}
