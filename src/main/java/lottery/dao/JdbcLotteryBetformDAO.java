package lottery.dao;

import lottery.model.BetForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * JdbcLotteryBetformDAO description
 *
 * @author johnson
 * @since 2015-06-26
 */
@Repository
public class JdbcLotteryBetformDAO implements ILotteryBetformDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public BetForm findByBetFormId(Integer betFormId) {
        return this.jdbcTemplate.queryForObject(
            "SELECT * FROM lottery_betForm WHERE betForm_id=?",
            new BetFormMapper(),
            betFormId
        );
    }

    private final class BetFormMapper implements RowMapper<BetForm> {

        @Override
        public BetForm mapRow(ResultSet rs, int rownum) throws SQLException {
            BetForm betform = new BetForm();
            betform.setBetFormId(rs.getInt("betForm_id"));
            betform.setBetId(rs.getShort("bet_id"));
            betform.setBetItem(rs.getString("betItem"));
            betform.setBetOptionClosed("betOptionClosed");
            betform.setRateModify(rs.getString("rateModify"));
            betform.setRateNotice(rs.getString("rateNotice"));
            betform.setUpCEO(rs.getLong("upCEO"));
            betform.setDownCEO(rs.getLong("downCEO"));
            betform.setUpShareown(rs.getLong("upShareown"));
            betform.setDownShareown(rs.getLong("downShareown"));
            betform.setUpAgent(rs.getLong("upAgent"));
            betform.setDownAgent(rs.getLong("downAgent"));
            betform.setPercentUpCEO(rs.getDouble("percentUpCEO"));
            betform.setPercentDownCEO(rs.getDouble("percentDownCEO"));
            betform.setPercentUpShareown(rs.getDouble("percentUpShareown"));
            betform.setPercentDownShareown(rs.getDouble("percentDownShareown"));
            betform.setPercentUpAgent(rs.getDouble("percentUpAgent"));
            betform.setPercentDownAgent(rs.getDouble("percentDownAgent"));
            betform.setUnitLimit(rs.getString("unitLimit"));
            betform.setBetModeId(rs.getShort("bet_mode_id"));
            return betform;
        }
    }
}
