package lottery.dao;

import lottery.model.BetForm;

/**
 * lottery_betForm的DAO interface
 *
 * @author johnson
 * @since 2015-06-26
 */
public interface ILotteryBetformDAO {
    /**
     * 依據betFormId撈betForm
     *
     * @param betFormId
     * @return {@link BetForm}
     */
    BetForm findByBetFormId(Integer betFormId);
}
