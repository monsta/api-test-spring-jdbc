package lottery.dao;

import lottery.model.Nurse;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * class description of NurseDaoJpa
 *
 * @author johnson
 */
//@Repository
public class NurseDaoJpa extends AbstractJpaDao<Nurse> implements INurseDao {

    @PersistenceContext
    private EntityManager em;

    public NurseDaoJpa() {
        super();
        setClazz(Nurse.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
