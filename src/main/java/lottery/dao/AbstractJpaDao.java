package lottery.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

/**
 * copy form Baeldung
 *
 * @author johnson
 * @since 2015-03-02
 */
public class AbstractJpaDao<T extends Serializable> {

    private Class<T> clazz;

    @PersistenceContext
    private EntityManager entityManager;

    public final void setClazz(final Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }

    public T findOne(final Integer id) {
        return entityManager.find(clazz, id);
    }

    public List<T> findByIdIn(final List<Integer> ids) {
        Query query = entityManager.createQuery("from " + clazz.getName() + " where id IN :ids ");
        query.setParameter("ids", ids);
        return query.getResultList();
    }

    public List<T> findAll() {
        return entityManager.createQuery("from " + clazz.getName()).getResultList();
    }

    public void create(final T entity) {
        entityManager.persist(entity);
    }

    public T update(final T entity) {
        return entityManager.merge(entity);
    }

    public void delete(final T entity) {
        entityManager.remove(entity);
    }

    public void deleteById(final Integer entityId) {
        final T entity = findOne(entityId);
        delete(entity);
    }
}
