package lottery.dao;

import lottery.model.Nurse;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * class description of INurseDao
 *
 * @author johnson
 */
public interface INurseDao {
    List<Nurse> findAll();

    void create(Nurse entity);

    Nurse update(Nurse entity);

    void delete(Nurse entity);

    Nurse findOne(Integer nurseId);

    EntityManager getEntityManager();
}
