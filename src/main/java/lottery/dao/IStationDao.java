package lottery.dao;

import lottery.model.Station;

import java.util.List;

/**
 * IStationDao description
 *
 * @author johnson
 * @since 2015-03-02
 */
public interface IStationDao {

    List<Station> findAll();

    List<Station> findByIdIn(List<Integer> stationSelect);

    void create(Station station);

    Station update(Station entity);

    void delete(Station entity);

    Station findOne(Integer stationId);
}
