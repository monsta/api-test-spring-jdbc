package lottery.dao;

import lottery.model.Station;
import org.springframework.stereotype.Repository;

/**
 * StationDaoJpa description
 *
 * @author johnson
 * @since 2015-03-02
 */
//@Repository
public class StationDaoJpa extends AbstractJpaDao<Station> implements IStationDao {

    public StationDaoJpa() {
        super();
        setClazz(Station.class);
    }
}
