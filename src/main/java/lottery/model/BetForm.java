package lottery.model;

import java.io.Serializable;

/**
 * lottery_betForm
 *
 * @author johnson
 * @since 2015-06-22
 */
public class BetForm implements Serializable {

    private long betFormId;
    private long scheduleId;
    private short betId;
    private double eachPayable;
    private double eachUnit;
    private double rate;
    private String betItem;
    private String rateModify;
    private String rateNotice;
    private String betOptionClosed;
    private long upCEO;
    private long downCEO;
    private long upShareown;
    private long downShareown;
    private long upAgent;
    private long downAgent;
    private double percentUpCEO;
    private double percentDownCEO;
    private double percentUpShareown;
    private double percentDownShareown;
    private double percentUpAgent;
    private double percentDownAgent;
    private String unitLimit;
    private short betModeId;

    public long getBetFormId() {
        return betFormId;
    }

    public void setBetFormId(long betFormId) {
        this.betFormId = betFormId;
    }

    public long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public short getBetId() {
        return betId;
    }

    public void setBetId(short betId) {
        this.betId = betId;
    }

    public double getEachPayable() {
        return eachPayable;
    }

    public void setEachPayable(double eachPayable) {
        this.eachPayable = eachPayable;
    }

    public double getEachUnit() {
        return eachUnit;
    }

    public void setEachUnit(double eachUnit) {
        this.eachUnit = eachUnit;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getBetItem() {
        return betItem;
    }

    public void setBetItem(String betItem) {
        this.betItem = betItem;
    }

    public String getRateModify() {
        return rateModify;
    }

    public void setRateModify(String rateModify) {
        this.rateModify = rateModify;
    }

    public String getRateNotice() {
        return rateNotice;
    }

    public void setRateNotice(String rateNotice) {
        this.rateNotice = rateNotice;
    }

    public String getBetOptionClosed() {
        return betOptionClosed;
    }

    public void setBetOptionClosed(String betOptionClosed) {
        this.betOptionClosed = betOptionClosed;
    }

    public long getUpCEO() {
        return upCEO;
    }

    public void setUpCEO(long upCEO) {
        this.upCEO = upCEO;
    }

    public long getDownCEO() {
        return downCEO;
    }

    public void setDownCEO(long downCEO) {
        this.downCEO = downCEO;
    }

    public long getUpShareown() {
        return upShareown;
    }

    public void setUpShareown(long upShareown) {
        this.upShareown = upShareown;
    }

    public long getDownShareown() {
        return downShareown;
    }

    public void setDownShareown(long downShareown) {
        this.downShareown = downShareown;
    }

    public long getUpAgent() {
        return upAgent;
    }

    public void setUpAgent(long upAgent) {
        this.upAgent = upAgent;
    }

    public long getDownAgent() {
        return downAgent;
    }

    public void setDownAgent(long downAgent) {
        this.downAgent = downAgent;
    }

    public double getPercentUpCEO() {
        return percentUpCEO;
    }

    public void setPercentUpCEO(double percentUpCEO) {
        this.percentUpCEO = percentUpCEO;
    }

    public double getPercentDownCEO() {
        return percentDownCEO;
    }

    public void setPercentDownCEO(double percentDownCEO) {
        this.percentDownCEO = percentDownCEO;
    }

    public double getPercentUpShareown() {
        return percentUpShareown;
    }

    public void setPercentUpShareown(double percentUpShareown) {
        this.percentUpShareown = percentUpShareown;
    }

    public double getPercentDownShareown() {
        return percentDownShareown;
    }

    public void setPercentDownShareown(double percentDownShareown) {
        this.percentDownShareown = percentDownShareown;
    }

    public double getPercentUpAgent() {
        return percentUpAgent;
    }

    public void setPercentUpAgent(double percentUpAgent) {
        this.percentUpAgent = percentUpAgent;
    }

    public double getPercentDownAgent() {
        return percentDownAgent;
    }

    public void setPercentDownAgent(double percentDownAgent) {
        this.percentDownAgent = percentDownAgent;
    }

    public String getUnitLimit() {
        return unitLimit;
    }

    public void setUnitLimit(String unitLimit) {
        this.unitLimit = unitLimit;
    }

    public short getBetModeId() {
        return betModeId;
    }

    public void setBetModeId(short betModeId) {
        this.betModeId = betModeId;
    }
}
