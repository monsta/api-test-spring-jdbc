package lottery.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * class description of Nurse
 *
 * @author johnson
 */
//@Entity
//@Table(name = "nurse")
public class Nurse implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true)
    private String no;

    private String name;

    @Column(name = "updatetime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatetime;

    @PrePersist
    @PreUpdate
    void beforePersist() {
        this.updatetime = new Date();
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "station_nurse",
            joinColumns = {
                @JoinColumn(name = "nurse_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "station_id", referencedColumnName = "id")})
    private Set<Station> stations = new HashSet<>(0);

    @Transient
    private Date addStationTime;

    public Nurse() {
    }

    public Nurse(String no, String name) {
        this.no = no;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getUpdatetimeFormated() {
        return DateTimeFormatter.ISO_DATE_TIME.format(
                LocalDateTime.ofInstant(getUpdatetime().toInstant(), ZoneId.systemDefault())
        );
    }

    public Set<Station> getStations() {
        return stations;
    }

    public void setStations(Set<Station> stations) {
        this.stations = stations;
    }

    public Date getAddStationTime() {
        return addStationTime;
    }

    public String getAddStationTimeFormated() {
        return DateTimeFormatter.ISO_DATE_TIME.format(
                LocalDateTime.ofInstant(getAddStationTime().toInstant(), ZoneId.systemDefault())
        );
    }

    public void setAddStationTime(Date addStationTime) {
        this.addStationTime = addStationTime;
    }
}
