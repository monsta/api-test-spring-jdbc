package lottery.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * class description of Station
 *
 * @author johnson
 */
//@Entity
//@Table(name = "station")
public class Station implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "updatetime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatetime;

    @PrePersist
    @PreUpdate
    void beforePersist() {
        this.updatetime = new Date();
    }

//    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "stations")
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "station_nurse",
        joinColumns = {
                @JoinColumn(name = "station_id", referencedColumnName = "id")},
        inverseJoinColumns = {
                @JoinColumn(name = "nurse_id", referencedColumnName = "id")})
    private Set<Nurse> nurses = new HashSet<>(0);

    /**
     * default constructor
     */
    public Station() {
        super();
    }

    /**
     * constructor
     *
     * @param id
     * @param name
     */
    public Station(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * constructor
     *
     * @param name
     */
    public Station(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public String getUpdatetimeFormated() {
        return DateTimeFormatter.ISO_DATE_TIME.format(
            LocalDateTime.ofInstant(getUpdatetime().toInstant(), ZoneId.systemDefault())
        );
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Set<Nurse> getNurses() {
        return nurses;
    }

    public void setNurses(Set<Nurse> nurses) {
        this.nurses = nurses;
    }
}
