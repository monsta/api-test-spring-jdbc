package lottery.controller;

import lottery.model.Nurse;
import lottery.model.Station;
import lottery.service.BionimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;
import java.util.stream.Collectors;

/**
 * class description of NurseController
 *
 * @author johnson
 */
@Controller
public class NurseController {

    private static final Logger LOG = LoggerFactory.getLogger(NurseController.class);
    private BionimeService bionimeService;

    @Autowired
    public NurseController(BionimeService bionimeService) {
        this.bionimeService = bionimeService;
    }

    @RequestMapping(value = "/nurse/list", method = RequestMethod.GET)
    public String listNurse(Model model) {
        model.addAttribute("nurseList", this.bionimeService.getAllNurse());
        return "listNurse";
    }

    @RequestMapping(value = "/nurse/new", method = RequestMethod.GET)
    public String toNewNursePage(Model model) {
        model.addAttribute("stationList", bionimeService.getAllStation());
        return "newNurse";
    }

    @RequestMapping(value = "/nurse/new", method = RequestMethod.POST)
    public String addNurse(@RequestParam String no, @RequestParam String name,
                           @RequestParam(required = false) Integer[] stationSelect,
                           RedirectAttributes redirectAtts) {

        try {
            this.bionimeService.addNurse(no, name, convertStationSelectArrayToList(stationSelect));
            redirectAtts.addFlashAttribute("message", FlashMessage.NEW_NURSE_SUCCESS);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            redirectAtts.addFlashAttribute("message", FlashMessage.NEW_NURSE_FAIL);
        }

        return "redirect:/nurse/list";
    }

    @RequestMapping(value = "/nurse/view/{nurseId}", method = RequestMethod.GET)
    public String viewNurse(@PathVariable Integer nurseId, Model model) {
        Nurse nurse = this.bionimeService.getNurse(nurseId);
        model.addAttribute("nurse", nurse);
        model.addAttribute("stationList", this.bionimeService.getAllStation());

        //前端判斷是否要selected
        Set<Integer> nurseStationsId = new HashSet<>();
        Set<Station> nurseStations = nurse.getStations();
        if(nurseStations != null && !nurseStations.isEmpty()) {
            nurseStationsId.addAll(
                    nurseStations.stream()
                            .map(Station::getId)
                            .collect(Collectors.toSet()));
        }

        model.addAttribute("nurseStationsId", nurseStationsId);
        return "viewNurse";
    }

    @RequestMapping(value = "/nurse/update/{nurseId}", method = RequestMethod.POST)
    public String updateNurse(@PathVariable Integer nurseId, @RequestParam String no,
                              @RequestParam String name,
                              @RequestParam(required = false) Integer[] stationSelect,
                              RedirectAttributes redirectAtts) {

        if (this.bionimeService.updateNurse(nurseId, no, name, convertStationSelectArrayToList(stationSelect))) {
            redirectAtts.addFlashAttribute("message", FlashMessage.UPDATE_NURSE_SUCCESS);
        } else {
            redirectAtts.addFlashAttribute("message", FlashMessage.UPDATE_NURSE_FAIL);
        }
        return "redirect:/nurse/list";
    }

    private List<Integer> convertStationSelectArrayToList(Integer[] stationSelect) {
        List<Integer> selectedStationIds = new ArrayList<>();
        if (stationSelect != null) {
            selectedStationIds = Arrays.asList(stationSelect);
        }
        return selectedStationIds;
    }

    @RequestMapping(value = "/nurse/delete/{nurseId}", method = RequestMethod.POST)
    public String deleteNurse(@PathVariable Integer nurseId, RedirectAttributes redirectAtts) {
        this.bionimeService.deleteNurse(nurseId);
        redirectAtts.addFlashAttribute("message", FlashMessage.DEL_NURSE_SUCCESS);
        return "redirect:/nurse/list";
    }
}
