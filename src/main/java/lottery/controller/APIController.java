package lottery.controller;

import lottery.model.BetForm;
import lottery.service.LotteryService;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * APIController description
 *
 * @author johnson
 * @since 2015-06-22
 */
@RestController
public class APIController {

    private static final Logger LOG = LoggerFactory.getLogger(APIController.class);
    private LotteryService lotteryService;

    @Autowired
    public APIController(LotteryService lotteryService) {
        this.lotteryService = lotteryService;
    }

    @RequestMapping(value = "/ll", method = RequestMethod.POST)
    public String lotteryLimit(@RequestParam String input) throws IOException {
        Map<String, String> resultMap = new HashMap<>();

        Map<String, Object> inputMap = convertInputToMap(lotteryService, input);
        inputMap.keySet().stream()
            .forEach(betFormId -> {
                boolean result = false;
                try {
                    result = lotteryService.processBetform(betFormId);
                } catch (IOException e) {
                    LOG.error("exception during betForm processing: "+betFormId, e);
                }
                resultMap.put(betFormId, String.valueOf(result));
            });

        return new ObjectMapper().writeValueAsString(resultMap);
    }

    /**
     * 將api input轉為Map<String, String>
     * 若input json字串無法轉，將回傳empty map
     *
     * @param lotteryService
     * @param input API輸入字串，ex:"{"1":"{\"01x02x03\":\"40.0000\"}","2":"","3":"{\"01x02x04\":\"40.0000\"}"}"
     * @return
     */
    public Map<String, Object> convertInputToMap(LotteryService lotteryService, String input) {
        Object mapResult = mapJsonStringInputToType(input, new
            TypeReference<HashMap<String, Object>>() {
            });
        if(mapResult != null) {
            return (Map<String, Object>) mapResult;
        } else {
            return Collections.emptyMap();
        }
    }

    private Object mapJsonStringInputToType(String json, TypeReference<?> typeRef) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, typeRef);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}
