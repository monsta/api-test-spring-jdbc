package lottery.controller;

import lottery.service.BionimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * class description of StationController
 *
 * @author johnson
 */
@Controller
public class StationController {

    private static final Logger LOG = LoggerFactory.getLogger(StationController.class);
    private BionimeService bionimeService;

    @Autowired
    public StationController(BionimeService bionimeService) {
        this.bionimeService = bionimeService;
    }

    @RequestMapping(value = "/station/new", method = RequestMethod.GET)
    public String toNewStationPage() {
        return "newStation";
    }

    @RequestMapping(value = "/station/list", method = RequestMethod.GET)
    public String toListStation(Model model) {
        model.addAttribute("stationList", bionimeService.getAllStation());
        return "listStation";
    }

    @RequestMapping(value = "/station/new", method = RequestMethod.POST)
    public String addNewStation(@RequestParam String stationName, RedirectAttributes redirectAtt) {
        if(this.bionimeService.addStation(stationName)){
            LOG.warn("add station success~~!");
            redirectAtt.addFlashAttribute("message", FlashMessage.NEW_STATION_SUCCESS);
        } else {
            LOG.warn("add station failed~~!");
            redirectAtt.addFlashAttribute("message", FlashMessage.NEW_STATION_FAIL);
        }
        return "redirect:/station/list";
    }

    @RequestMapping(value = "/station/view/{stationId}", method = RequestMethod.GET)
    public String viewStation(@PathVariable Integer stationId, Model model) {
        model.addAttribute("station", bionimeService.getStation(stationId));
        return "viewStation";
    }

    @RequestMapping(value = "/station/update/{stationId}", method = RequestMethod.POST)
    public String updateStation(@PathVariable Integer stationId, @RequestParam String stationName) {
        this.bionimeService.updateStationName(stationId, stationName);
        return "redirect:/station/list";
    }

    @RequestMapping(value = "/station/delete/{stationId}", method = RequestMethod.POST)
    public String deleteStation(@PathVariable Integer stationId) {
        this.bionimeService.deleteStation(stationId);
        return "redirect:/station/list";
    }
}
