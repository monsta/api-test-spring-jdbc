package lottery.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
public class IndexController {

    @RequestMapping(value = "/", method = GET)
    public String indexAction(Model model) {
        return "index";
    }
}
