package lottery.controller;

public enum FlashMessage {
    NEW_STATION_SUCCESS("新增站點成功"),
    NEW_STATION_FAIL("新增站點失敗"),
    NEW_NURSE_SUCCESS("新增護士成功"),
    NEW_NURSE_FAIL("新增護士失敗"),
    UPDATE_NURSE_SUCCESS("修改護士成功"),
    UPDATE_NURSE_FAIL("修改護士失敗"),
    DEL_NURSE_SUCCESS("刪除護士成功");

    private final String messageString;

    FlashMessage(String messageString) {
        this.messageString = messageString;
    }

    public String getMessageString() {
        return messageString;
    }
}
