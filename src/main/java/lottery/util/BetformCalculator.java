package lottery.util;

import com.google.common.collect.Lists;
import lottery.model.Combination;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

/**
 * BetformCalculator description
 *
 * @author johnson
 * @since 2015-06-26
 */
@Component
public class BetformCalculator {

    //bet_id和幾星的mapping
    private Map<Short, Integer> betIdMapping;

    public BetformCalculator() {
        this.betIdMapping = new HashMap<>();
        betIdMapping.put((short)5, 2);  //bet_id:5-->2星
        betIdMapping.put((short)6, 3);
        betIdMapping.put((short)7, 4);
        betIdMapping.put((short)8, 5);
    }

    public void doSomething() {
        //TODO
    }

    /**
     * 從betForm的betItem字串，去計算取得「連碰」的組合
     * ex:["01","11","21","31","41"]取2星，會得到list of {@link Combination}</br>
     * 內容為：[1,11][1,21][1,31][1,41][11,21][11,31][11,41][21,31][21,41][31,41]
     *
     * @param betItem 資料庫的lottery_betform.betitem欄位內容
     * @param betId 2~5星的哪一種
     * @return 由 {@link Combination}組成的List
     */
    public List<Combination> unionCombination(String betItem, short betId) throws IOException {
        List<Combination> rtn = null;

        if(StringUtils.isNotEmpty(betItem)) {
            Set<Integer> numbers = unionBetItemToSet(betItem);
            rtn = getCombinationOutOf(numbers, betIdMapping.get(betId));
        } else {
            throw new IllegalArgumentException("betItem is empty!!");
        }

        return rtn;
    }

    /**
     * 將連碰的betItem字串，轉成Integer的Set
     *
     * @param jsonStringArray
     * @return
     */
    private Set<Integer> unionBetItemToSet(String jsonStringArray) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonStringArray, new TypeReference<Set<Integer>>() {
        });
        /*Set<Integer> numbers = new HashSet<>();
        //把所有雙引號拿掉
        String betItemRemoveDoubleQuote = StringUtils.remove(betItem, "\"");
        //把最前面[和最後面]拿掉
        String betNumbersWithComma = betItemRemoveDoubleQuote.substring(1, betItemRemoveDoubleQuote.length() - 1);
        String[] betNumbers = StringUtils.split(betNumbersWithComma, ",");
        Arrays.asList(betNumbers).forEach(betnum -> {
            numbers.add(NumberUtils.toInt(betnum));
        });
        return numbers;*/
    }

    /**
     * 從betForm的betItem字串，去計算取得「柱碰」的組合
     * ex:[["01","11"],["21","31","41"]]取2星，會得到list of {@link Combination}</br>
     * 內容為：[1,21][1,31][1,41][11,21][11,31][11,41]
     *
     * @param betItem 資料庫的lottery_betform.betitem欄位內容
     * @param betId 2~5星的哪一種
     * @return 由 {@link Combination}組成的List
     */
    public List<Combination> crossCombination(String betItem, short betId) {

        return null;
    }

    /**
     * 產生所有連碰組合，每個組合儲存在{@link Combination}物件裡面
     *
     * @param numbers 所有選擇的號碼
     * @param stars   要產生幾星
     * @return
     */
    private <T> List<Combination> getCombinationOutOf(Set<T> numbers, int stars) {
        List<Combination> result = Lists.newArrayList();
        List<T> selectedNumbers = Lists.newArrayList(numbers);

        // 取得index的所有組合，ex:c3取2-->[0,1],[0,2],[1,2]
        Iterator<int[]> indexCombinationIterator = CombinatoricsUtils.combinationsIterator(
            numbers.size(), stars);

        indexCombinationIterator.forEachRemaining(next -> {
            Combination combination = new Combination(stars);
            for (int i = 0; i < next.length; i++) {
                combination.add(selectedNumbers.get(next[i]));
            }
            result.add(combination);
        });

        return result;
    }
}
