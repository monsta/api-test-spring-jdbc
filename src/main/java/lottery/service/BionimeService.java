package lottery.service;

import lottery.dao.INurseDao;
import lottery.dao.IStationDao;
import lottery.model.Nurse;
import lottery.model.Station;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * class description of BionimeService
 *
 * @author johnson
 */
//@Service
public class BionimeService {

    private static final Logger LOG = LoggerFactory.getLogger(BionimeService.class);
    @Autowired
    private IStationDao stationDao;
    @Autowired
    private INurseDao nurseDao;

    /**
     * get all station
     *
     * @return
     */
    public List<Station> getAllStation() {
        return stationDao.findAll();
    }

    /**
     * create one station, the station id and station updatetime would auto create by default
     *
     * @param stationName
     * @return
     */
    @Transactional
    public boolean addStation(String stationName) {
        stationDao.create(new Station(stationName));
        return true;
    }

    @Transactional(readOnly = true)
    public Station getStation(Integer stationId) {
        Station station = stationDao.findOne(stationId);
        Set<Nurse> nursesInThisStation = station.getNurses();

        //get nurse add into station time
        if (nursesInThisStation != null && !nursesInThisStation.isEmpty()) {
            for (Nurse nurse : nursesInThisStation) {
                Query query = nurseDao.getEntityManager().createNativeQuery("select add_nurse_time from " +
                        "station_nurse " +
                        " where station_id=:station_id and nurse_id=:nurse_id");
                query.setParameter("station_id", station.getId())
                        .setParameter("nurse_id", nurse.getId());
                Date addDatetime = (Date) query.getResultList().get(0);
                nurse.setAddStationTime(addDatetime);
            }
        }
        return station;
    }

    @Transactional
    public boolean updateStationName(Integer stationId, String newStationName) {
        Station targetStation = stationDao.findOne(stationId);
        targetStation.setName(newStationName);
        stationDao.update(targetStation);
        return true;
    }

    @Transactional
    public boolean deleteStation(Integer stationId) {
        Station station = stationDao.findOne(stationId);

        //avoid foreign key constraint
        station.setNurses(null);
        stationDao.update(station);

        stationDao.delete(station);
        return true;
    }

    @Transactional(readOnly = true)
    public List<Nurse> getAllNurse() {
        return nurseDao.findAll();
    }

    @Transactional
    public void addNurse(String no, String name, List<Integer> stationSelect) {
        Nurse nurse = new Nurse(no, name);

        if (stationSelect != null && !stationSelect.isEmpty()) {
            //get selected station objects by selected IDs and setStations
            List<Station> stationSelected = this.stationDao.findByIdIn(stationSelect);
            nurse.getStations().addAll(stationSelected);
        }

        nurseDao.create(nurse);
    }

    @Transactional(readOnly = true)
    public Nurse getNurse(Integer nurseId) {
        return nurseDao.findOne(nurseId);
    }

    @Transactional
    public boolean updateNurse(Integer nurseId, String no, String name, List<Integer> stationSelect) {
        boolean updateSuccess = false;

        Nurse nurse = this.nurseDao.findOne(nurseId);
        nurse.setNo(no);
        nurse.setName(name);

        if (stationSelect == null || stationSelect.isEmpty()) {
            if (nurse.getStations() != null && !nurse.getStations().isEmpty()) {
                nurse.getStations().clear();
            }
        } else {
            //get selected station objects by selected IDs and setStations
            List<Station> stationSelected = this.stationDao.findByIdIn(stationSelect);
            nurse.getStations().clear();
            nurse.getStations().addAll(stationSelected);
        }

        try {
            this.nurseDao.update(nurse);
            updateSuccess = true;
        } catch (Exception dae) {
            //maybe unique constraint violated
            LOG.error(dae.getMessage(), dae);
        }
        return updateSuccess;
    }

    @Transactional
    public void deleteNurse(Integer nurseId) {
        Nurse nurse = nurseDao.findOne(nurseId);

        //avoid foreign key constraint
        nurse.setStations(null);
        nurseDao.update(nurse);

        this.nurseDao.delete(nurse);
    }
}
