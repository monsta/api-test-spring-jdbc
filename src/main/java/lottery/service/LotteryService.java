package lottery.service;

import lottery.dao.ILotteryBetformDAO;
import lottery.model.BetForm;
import lottery.model.Combination;
import lottery.util.BetformCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * LotteryService description
 *
 * @author johnson
 * @since 2015-06-22
 */
@Service
public class LotteryService {

    private static final Logger LOG = LoggerFactory.getLogger(LotteryService.class);
    private static final short BET_MODE_UNION = 1;
    private static final short BET_MODE_CORSS = 2;
    private final ILotteryBetformDAO betFormDAO;
    private final BetformCalculator betformCalculator;

    @Autowired
    public LotteryService(ILotteryBetformDAO betformDAO, BetformCalculator betformCalculator) {
        this.betFormDAO = betformDAO;
        this.betformCalculator = betformCalculator;
    }

    /**
     * 依據betFormId取得betForm
     *
     * @param betFormId
     * @return
     */
    public boolean processBetform(String betFormId) throws IOException {
        boolean rtn = false;
        //撈betform
        BetForm betForm = betFormDAO.findByBetFormId(Integer.valueOf(betFormId));

        if(betForm != null) {
            //計算碰組合
            List<Combination> combinations;
            if(betForm.getBetModeId() == BET_MODE_UNION) {
                combinations = betformCalculator.unionCombination(betForm.getBetItem(), betForm.getBetId());
            } else if(betForm.getBetModeId() == BET_MODE_CORSS) {
                combinations = betformCalculator.crossCombination(betForm.getBetItem(), betForm.getBetId());
            }
        } else {
            LOG.warn("betForm query result is null!! betFormID: " + betFormId);
        }

        return rtn;
    }
}
