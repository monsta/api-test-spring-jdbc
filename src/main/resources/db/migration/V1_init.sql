/*for mysql*/
CREATE TABLE `bionime`.`station` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(10) NULL,
  `updatetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  );
  
CREATE TABLE `bionime`.`nurse` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `no` VARCHAR(10) NULL,
  `name` VARCHAR(10) NULL,
  `updatetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`no`)
  );

CREATE TABLE `bionime`.``station_nurse` (
    `station_id` INT NOT NULL,
    `nurse_id` INT NOT NULL,
	  `add_nurse_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`station_id`, `nurse_id`),
    INDEX `IX_STATION` (`station_id`),
    CONSTRAINT `FK_STATION` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`),
    CONSTRAINT `FK_NURSE` FOREIGN KEY (`nurse_id`) REFERENCES `nurse` (`id`)
  );

-- for test
insert into station (name) values('stationAA');
insert into station (name) values('stationBB');